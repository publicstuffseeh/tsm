﻿namespace TravelingSalesMan
{
    partial class TSMGui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OpenButton = new System.Windows.Forms.Button();
            this.AddPointButton = new System.Windows.Forms.Button();
            this.labelNord = new System.Windows.Forms.Label();
            this.labelOst = new System.Windows.Forms.Label();
            this.textBoxX = new System.Windows.Forms.TextBox();
            this.textBoxY = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ViewPointsButton = new System.Windows.Forms.Button();
            this.ViewDistancesButton = new System.Windows.Forms.Button();
            this.CalcDistancesButton = new System.Windows.Forms.Button();
            this.CalcRouteButton = new System.Windows.Forms.Button();
            this.TSMallPointsButton = new System.Windows.Forms.Button();
            this.TSMstationenButton = new System.Windows.Forms.Button();
            this.TSMnextPointsButton = new System.Windows.Forms.Button();
            this.TSMonlyClosestButton = new System.Windows.Forms.Button();
            this.stationenAnzahlTextBox = new System.Windows.Forms.TextBox();
            this.textBoxUrl = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.GroupBoxDatenbankOperationen = new System.Windows.Forms.GroupBox();
            this.NewDataBaseButton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.GroupBoxTSM = new System.Windows.Forms.GroupBox();
            this.labelN = new System.Windows.Forms.Label();
            this.groupBoxDatenbankFunktionen = new System.Windows.Forms.GroupBox();
            this.textBoxStationsName = new System.Windows.Forms.TextBox();
            this.labelPunktName = new System.Windows.Forms.Label();
            this.groupBoxBrowser = new System.Windows.Forms.GroupBox();
            this.NavigateButton = new System.Windows.Forms.Button();
            this.labelUrl = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.GroupBoxDatenbankOperationen.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.GroupBoxTSM.SuspendLayout();
            this.groupBoxDatenbankFunktionen.SuspendLayout();
            this.groupBoxBrowser.SuspendLayout();
            this.SuspendLayout();
            //
            // OpenButton
            //
            this.OpenButton.Location = new System.Drawing.Point(6, 19);
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Size = new System.Drawing.Size(130, 23);
            this.OpenButton.TabIndex = 0;
            this.OpenButton.Text = "Open Database";
            this.OpenButton.UseVisualStyleBackColor = true;
            this.OpenButton.Click += new System.EventHandler(this.OpenButtonClick);
            //
            // AddPointButton
            //
            this.AddPointButton.Location = new System.Drawing.Point(231, 33);
            this.AddPointButton.Name = "AddPointButton";
            this.AddPointButton.Size = new System.Drawing.Size(130, 23);
            this.AddPointButton.TabIndex = 1;
            this.AddPointButton.Text = "Add Point";
            this.AddPointButton.UseVisualStyleBackColor = true;
            this.AddPointButton.Click += new System.EventHandler(this.AddPointButtonClick);
            //
            // labelNord
            //
            this.labelNord.AutoSize = true;
            this.labelNord.Location = new System.Drawing.Point(6, 18);
            this.labelNord.Name = "labelNord";
            this.labelNord.Size = new System.Drawing.Size(30, 13);
            this.labelNord.TabIndex = 2;
            this.labelNord.Text = "Nord";
            //
            // labelOst
            //
            this.labelOst.AutoSize = true;
            this.labelOst.Location = new System.Drawing.Point(125, 18);
            this.labelOst.Name = "labelOst";
            this.labelOst.Size = new System.Drawing.Size(23, 13);
            this.labelOst.TabIndex = 3;
            this.labelOst.Text = "Ost";
            //
            // textBoxX
            //
            this.textBoxX.Location = new System.Drawing.Point(6, 35);
            this.textBoxX.Name = "textBoxX";
            this.textBoxX.Size = new System.Drawing.Size(100, 20);
            this.textBoxX.TabIndex = 4;
            //
            // textBoxY
            //
            this.textBoxY.Location = new System.Drawing.Point(125, 35);
            this.textBoxY.Name = "textBoxY";
            this.textBoxY.Size = new System.Drawing.Size(100, 20);
            this.textBoxY.TabIndex = 5;
            //
            // dataGridView1
            //
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 101);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(504, 832);
            this.dataGridView1.TabIndex = 6;
            //
            // ViewPointsButton
            //
            this.ViewPointsButton.Location = new System.Drawing.Point(142, 19);
            this.ViewPointsButton.Name = "ViewPointsButton";
            this.ViewPointsButton.Size = new System.Drawing.Size(130, 23);
            this.ViewPointsButton.TabIndex = 7;
            this.ViewPointsButton.Text = "View Points";
            this.ViewPointsButton.UseVisualStyleBackColor = true;
            this.ViewPointsButton.Click += new System.EventHandler(this.ViewPointsButtonClick);
            //
            // ViewDistancesButton
            //
            this.ViewDistancesButton.Location = new System.Drawing.Point(142, 48);
            this.ViewDistancesButton.Name = "ViewDistancesButton";
            this.ViewDistancesButton.Size = new System.Drawing.Size(130, 23);
            this.ViewDistancesButton.TabIndex = 8;
            this.ViewDistancesButton.Text = "View Distances";
            this.ViewDistancesButton.UseVisualStyleBackColor = true;
            this.ViewDistancesButton.Click += new System.EventHandler(this.ViewDistancesButtonClick);
            //
            // CalcDistancesButton
            //
            this.CalcDistancesButton.Location = new System.Drawing.Point(423, 19);
            this.CalcDistancesButton.Name = "CalcDistancesButton";
            this.CalcDistancesButton.Size = new System.Drawing.Size(130, 23);
            this.CalcDistancesButton.TabIndex = 9;
            this.CalcDistancesButton.Text = "Calculate Distances";
            this.CalcDistancesButton.UseVisualStyleBackColor = true;
            this.CalcDistancesButton.Click += new System.EventHandler(this.CalcDistancesButtonClick);
            //
            // CalcRouteButton
            //
            this.CalcRouteButton.Location = new System.Drawing.Point(423, 48);
            this.CalcRouteButton.Name = "CalcRouteButton";
            this.CalcRouteButton.Size = new System.Drawing.Size(130, 23);
            this.CalcRouteButton.TabIndex = 10;
            this.CalcRouteButton.Text = "Calculate Route";
            this.CalcRouteButton.UseVisualStyleBackColor = true;
            this.CalcRouteButton.Click += new System.EventHandler(this.CalcRouteButtonClick);
            //
            // TSMallPointsButton
            //
            this.TSMallPointsButton.Location = new System.Drawing.Point(6, 48);
            this.TSMallPointsButton.Name = "TSMallPointsButton";
            this.TSMallPointsButton.Size = new System.Drawing.Size(130, 23);
            this.TSMallPointsButton.TabIndex = 12;
            this.TSMallPointsButton.Text = " TSM All Points";
            this.TSMallPointsButton.UseVisualStyleBackColor = true;
            this.TSMallPointsButton.Click += new System.EventHandler(this.TSMallPointsButtonClick);
            //
            // TSMstationenButton
            //
            this.TSMstationenButton.Location = new System.Drawing.Point(6, 19);
            this.TSMstationenButton.Name = "TSMstationenButton";
            this.TSMstationenButton.Size = new System.Drawing.Size(130, 23);
            this.TSMstationenButton.TabIndex = 14;
            this.TSMstationenButton.Text = "TSM Nur Stationen";
            this.TSMstationenButton.UseVisualStyleBackColor = true;
            this.TSMstationenButton.Click += new System.EventHandler(this.TSMstationenButtonClick);
            //
            // TSMnextPointsButton
            //
            this.TSMnextPointsButton.Location = new System.Drawing.Point(281, 48);
            this.TSMnextPointsButton.Name = "TSMnextPointsButton";
            this.TSMnextPointsButton.Size = new System.Drawing.Size(130, 23);
            this.TSMnextPointsButton.TabIndex = 15;
            this.TSMnextPointsButton.Text = "TSM N-Nächste Punkte";
            this.TSMnextPointsButton.UseVisualStyleBackColor = true;
            this.TSMnextPointsButton.Click += new System.EventHandler(this.TSMnextPointsButtonClick);
            //
            // TSMonlyClosestButton
            //
            this.TSMonlyClosestButton.Location = new System.Drawing.Point(281, 17);
            this.TSMonlyClosestButton.Name = "TSMonlyClosestButton";
            this.TSMonlyClosestButton.Size = new System.Drawing.Size(130, 23);
            this.TSMonlyClosestButton.TabIndex = 16;
            this.TSMonlyClosestButton.Text = "TSM Eleminiere N";
            this.TSMonlyClosestButton.UseVisualStyleBackColor = true;
            //
            // stationenAnzahlTextBox
            //
            this.stationenAnzahlTextBox.Location = new System.Drawing.Point(163, 20);
            this.stationenAnzahlTextBox.Name = "stationenAnzahlTextBox";
            this.stationenAnzahlTextBox.Size = new System.Drawing.Size(100, 20);
            this.stationenAnzahlTextBox.TabIndex = 17;
            //
            // textBoxUrl
            //
            this.textBoxUrl.Location = new System.Drawing.Point(6, 38);
            this.textBoxUrl.Name = "textBoxUrl";
            this.textBoxUrl.Size = new System.Drawing.Size(390, 20);
            this.textBoxUrl.TabIndex = 18;
            //
            // openFileDialog1
            //
            this.openFileDialog1.Filter = "Sqlite-Datenbanken|*.db3|Alle Dateien|*.*";
            //
            // GroupBoxDatenbankOperationen
            //
            this.GroupBoxDatenbankOperationen.Controls.Add(this.NewDataBaseButton);
            this.GroupBoxDatenbankOperationen.Controls.Add(this.OpenButton);
            this.GroupBoxDatenbankOperationen.Controls.Add(this.ViewPointsButton);
            this.GroupBoxDatenbankOperationen.Controls.Add(this.ViewDistancesButton);
            this.GroupBoxDatenbankOperationen.Location = new System.Drawing.Point(14, 12);
            this.GroupBoxDatenbankOperationen.Name = "GroupBoxDatenbankOperationen";
            this.GroupBoxDatenbankOperationen.Size = new System.Drawing.Size(285, 83);
            this.GroupBoxDatenbankOperationen.TabIndex = 19;
            this.GroupBoxDatenbankOperationen.TabStop = false;
            this.GroupBoxDatenbankOperationen.Text = "Datenbank";
            //
            // NewDataBaseButton
            //
            this.NewDataBaseButton.Location = new System.Drawing.Point(6, 48);
            this.NewDataBaseButton.Name = "NewDataBaseButton";
            this.NewDataBaseButton.Size = new System.Drawing.Size(129, 23);
            this.NewDataBaseButton.TabIndex = 9;
            this.NewDataBaseButton.Text = "New Database";
            this.NewDataBaseButton.UseVisualStyleBackColor = true;
            this.NewDataBaseButton.Click += new System.EventHandler(this.NewDataBaseButtonClick);
            //
            // statusStrip1
            //
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.toolStripProgressBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 939);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1884, 22);
            this.statusStrip1.TabIndex = 20;
            this.statusStrip1.Text = "statusStrip1";
            //
            // toolStripStatusLabel
            //
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            //
            // toolStripProgressBar
            //
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(100, 16);
            //
            // GroupBoxTSM
            //
            this.GroupBoxTSM.Controls.Add(this.labelN);
            this.GroupBoxTSM.Controls.Add(this.TSMallPointsButton);
            this.GroupBoxTSM.Controls.Add(this.TSMstationenButton);
            this.GroupBoxTSM.Controls.Add(this.TSMonlyClosestButton);
            this.GroupBoxTSM.Controls.Add(this.stationenAnzahlTextBox);
            this.GroupBoxTSM.Controls.Add(this.TSMnextPointsButton);
            this.GroupBoxTSM.Location = new System.Drawing.Point(1451, 12);
            this.GroupBoxTSM.Name = "GroupBoxTSM";
            this.GroupBoxTSM.Size = new System.Drawing.Size(421, 83);
            this.GroupBoxTSM.TabIndex = 21;
            this.GroupBoxTSM.TabStop = false;
            this.GroupBoxTSM.Text = "TSM";
            //
            // labelN
            //
            this.labelN.AutoSize = true;
            this.labelN.Location = new System.Drawing.Point(142, 24);
            this.labelN.Name = "labelN";
            this.labelN.Size = new System.Drawing.Size(15, 13);
            this.labelN.TabIndex = 22;
            this.labelN.Text = "N";
            //
            // groupBoxDatenbankFunktionen
            //
            this.groupBoxDatenbankFunktionen.Controls.Add(this.textBoxStationsName);
            this.groupBoxDatenbankFunktionen.Controls.Add(this.labelPunktName);
            this.groupBoxDatenbankFunktionen.Controls.Add(this.textBoxX);
            this.groupBoxDatenbankFunktionen.Controls.Add(this.AddPointButton);
            this.groupBoxDatenbankFunktionen.Controls.Add(this.labelNord);
            this.groupBoxDatenbankFunktionen.Controls.Add(this.labelOst);
            this.groupBoxDatenbankFunktionen.Controls.Add(this.CalcRouteButton);
            this.groupBoxDatenbankFunktionen.Controls.Add(this.textBoxY);
            this.groupBoxDatenbankFunktionen.Controls.Add(this.CalcDistancesButton);
            this.groupBoxDatenbankFunktionen.Location = new System.Drawing.Point(313, 12);
            this.groupBoxDatenbankFunktionen.Name = "groupBoxDatenbankFunktionen";
            this.groupBoxDatenbankFunktionen.Size = new System.Drawing.Size(559, 83);
            this.groupBoxDatenbankFunktionen.TabIndex = 22;
            this.groupBoxDatenbankFunktionen.TabStop = false;
            this.groupBoxDatenbankFunktionen.Text = "Datenbank Funktionen";
            //
            // textBoxStationsName
            //
            this.textBoxStationsName.Location = new System.Drawing.Point(42, 57);
            this.textBoxStationsName.Name = "textBoxStationsName";
            this.textBoxStationsName.Size = new System.Drawing.Size(183, 20);
            this.textBoxStationsName.TabIndex = 12;
            //
            // labelPunktName
            //
            this.labelPunktName.AutoSize = true;
            this.labelPunktName.Location = new System.Drawing.Point(6, 61);
            this.labelPunktName.Name = "labelPunktName";
            this.labelPunktName.Size = new System.Drawing.Size(35, 13);
            this.labelPunktName.TabIndex = 11;
            this.labelPunktName.Text = "Name";
            //
            // groupBoxBrowser
            //
            this.groupBoxBrowser.Controls.Add(this.button1);
            this.groupBoxBrowser.Controls.Add(this.NavigateButton);
            this.groupBoxBrowser.Controls.Add(this.labelUrl);
            this.groupBoxBrowser.Controls.Add(this.textBoxUrl);
            this.groupBoxBrowser.Location = new System.Drawing.Point(878, 12);
            this.groupBoxBrowser.Name = "groupBoxBrowser";
            this.groupBoxBrowser.Size = new System.Drawing.Size(552, 80);
            this.groupBoxBrowser.TabIndex = 23;
            this.groupBoxBrowser.TabStop = false;
            this.groupBoxBrowser.Text = "Browser";
            //
            // NavigateButton
            //
            this.NavigateButton.Location = new System.Drawing.Point(416, 35);
            this.NavigateButton.Name = "NavigateButton";
            this.NavigateButton.Size = new System.Drawing.Size(130, 23);
            this.NavigateButton.TabIndex = 11;
            this.NavigateButton.Text = "Goto URL";
            this.NavigateButton.UseVisualStyleBackColor = true;
            this.NavigateButton.Click += new System.EventHandler(this.NavigateButtonClick);
            //
            // labelUrl
            //
            this.labelUrl.AutoSize = true;
            this.labelUrl.Location = new System.Drawing.Point(7, 21);
            this.labelUrl.Name = "labelUrl";
            this.labelUrl.Size = new System.Drawing.Size(29, 13);
            this.labelUrl.TabIndex = 19;
            this.labelUrl.Text = "URL";
            //
            // saveFileDialog1
            //
            this.saveFileDialog1.Filter = "Sqlite-Datenbanken|*.db3|Alle Dateien|*.*";
            //
            // button1
            //
            this.button1.Location = new System.Drawing.Point(416, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            //
            // TSMGui
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1884, 961);
            this.Controls.Add(this.groupBoxBrowser);
            this.Controls.Add(this.groupBoxDatenbankFunktionen);
            this.Controls.Add(this.GroupBoxTSM);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.GroupBoxDatenbankOperationen);
            this.Controls.Add(this.dataGridView1);
            this.Name = "TSMGui";
            this.Text = "TravelingSalesMan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Fenster_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.GroupBoxDatenbankOperationen.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.GroupBoxTSM.ResumeLayout(false);
            this.GroupBoxTSM.PerformLayout();
            this.groupBoxDatenbankFunktionen.ResumeLayout(false);
            this.groupBoxDatenbankFunktionen.PerformLayout();
            this.groupBoxBrowser.ResumeLayout(false);
            this.groupBoxBrowser.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OpenButton;
        private System.Windows.Forms.Button AddPointButton;
        private System.Windows.Forms.Label labelNord;
        private System.Windows.Forms.Label labelOst;
        private System.Windows.Forms.TextBox textBoxX;
        private System.Windows.Forms.TextBox textBoxY;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button ViewPointsButton;
        private System.Windows.Forms.Button ViewDistancesButton;
        private System.Windows.Forms.Button CalcDistancesButton;
        private System.Windows.Forms.Button CalcRouteButton;
        private System.Windows.Forms.Button TSMallPointsButton;
        private System.Windows.Forms.Button TSMstationenButton;
        private System.Windows.Forms.Button TSMnextPointsButton;
        private System.Windows.Forms.Button TSMonlyClosestButton;
        private System.Windows.Forms.TextBox stationenAnzahlTextBox;
        private System.Windows.Forms.TextBox textBoxUrl;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox GroupBoxDatenbankOperationen;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
        private System.Windows.Forms.Button NewDataBaseButton;
        private System.Windows.Forms.GroupBox GroupBoxTSM;
        private System.Windows.Forms.Label labelN;
        private System.Windows.Forms.GroupBox groupBoxDatenbankFunktionen;
        private System.Windows.Forms.GroupBox groupBoxBrowser;
        private System.Windows.Forms.Button NavigateButton;
        private System.Windows.Forms.Label labelUrl;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.TextBox textBoxStationsName;
        private System.Windows.Forms.Label labelPunktName;
        private System.Windows.Forms.Button button1;
    }
}

