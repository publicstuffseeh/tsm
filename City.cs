﻿namespace TravelingSalesMan
{
    // Globales Object (im Namespace)

    class City
    {
        public double X { get; set; }
        public double Y { get; set; }
        public int Id { get; set; }
        static public double[,] distTable;

        // Distanz-Table jetzt als static in (allen) Städten
        static public void InitDistTable(int N)
        {
            distTable = new double[N, N];
        }

        public City(double x, double y, int id)
        {
            X = x;
            Y = y;
            Id = id;
        }

        public double Distance(City other)
        {
            return distTable[this.Id, other.Id];
        }
    }
}
