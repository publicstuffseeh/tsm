﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace TravelingSalesMan
{

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var programDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            var browser = BrowserType.Firefox;
            try
            {
                Gecko.Xpcom.Initialize(Path.Combine(programDirectory, "Firefox"));
            }
            catch (Exception)
            {
                // MessageBox.Show("Firefox-Browswer-Funktionalität nicht gegeben unter Linux at the moment");
                browser = BrowserType.Builtin;   // c# default form-Browser (internet-explorer?!)
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new TSMGui(browser));
        }
    }
}
