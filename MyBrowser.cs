﻿using System.Windows.Forms;

namespace TravelingSalesMan
{

    public enum BrowserType
    {
        Firefox,
        Builtin
    }

    class MyBrowser
    {
        private object myBrowser;
        private Form parentForm;   // TSMGui
        private readonly BrowserType browserType;
        private readonly string homepage = "https://classic-maps.openrouteservice.org/directions?n1=51.122704&n2=9.560337&n3=12&a=51.127384,9.548675,null,null&b=0&c=0&k1=en-US&k2=km";

        public MyBrowser(Form form, BrowserType browser)
        {
            browserType = browser;
            parentForm = form;
            if (browserType == BrowserType.Firefox)
            {
                myBrowser = new Gecko.GeckoWebBrowser
                {
                    ConsoleMessageEventReceivesConsoleLogCalls = true,
                    FrameEventsPropagateToMainWindow = false,
                    Location = new System.Drawing.Point(539, 101),
                    Name = "geckoWebBrowser1",
                    Size = new System.Drawing.Size(1333, 816),
                    TabIndex = 19,
                    UseHttpActivityObserver = false,
                };
                ((Gecko.GeckoWebBrowser)myBrowser).Navigated += new System.EventHandler<Gecko.GeckoNavigatedEventArgs>(UpdateUrl);
                parentForm.Controls.Add((Gecko.GeckoWebBrowser) myBrowser);
            }
            else
            {
                myBrowser = new WebBrowser
                {
                    Location = new System.Drawing.Point(539, 101),
                    MinimumSize = new System.Drawing.Size(20, 20),
                    Name = "webBrowser1",
                    Size = new System.Drawing.Size(1333, 816),
                    TabIndex = 19
                };
                form.Controls.Add((WebBrowser)myBrowser);
            }
            // Gleich auf Homepage gehen:
            Navigate(homepage);
        }

        public void Navigate(string url)
        {
            if (browserType == BrowserType.Firefox)
            {
                ((Gecko.GeckoWebBrowser)myBrowser).Navigate(url);
            }
            else
            {
                ((WebBrowser)myBrowser).Navigate(url);
            }
        }

        public void UpdateUrl(object sender, System.EventArgs e)
        {
            string myUrl = ((Gecko.GeckoWebBrowser)myBrowser).Url.ToString();
            ((TextBox)parentForm.Controls.Find("textBoxUrl", true)[0]).Text = myUrl;

            if (myUrl.Contains("classic-maps.openrouteservice.org/directions"))
            {
                string xy = myUrl.Substring(myUrl.IndexOf("a=") + 2);
                ((TextBox)parentForm.Controls.Find("textBoxX", true)[0]).Text = xy.Split(',')[0];
                ((TextBox)parentForm.Controls.Find("textBoxY", true)[0]).Text = xy.Split(',')[1];
            }
            // https://classic-maps.openrouteservice.org/directions?n1=51.130152&n2=9.55597&n3=16&a=51.129155,9.558856,null,null&b=2&c=0&k1=en-US&k2=km

            //parentForm.Controls["textBoxUrl"].Text = ((Gecko.GeckoWebBrowser)myBrowser).Url.ToString();
        }
    }
}
