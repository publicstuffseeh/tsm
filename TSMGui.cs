﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace TravelingSalesMan
{

    public partial class TSMGui : Form
    {
        private string dbName = "tsm.db3";

        private List<City> lCity;

        private Algorithms algos;
        private MyBrowser myBrowser;
        private SQL sql;

        public TSMGui(BrowserType browser)
        {
            InitializeComponent();
            myBrowser = new MyBrowser(this, browser);
            algos = new Algorithms();
            sql = new SQL();
        }

        #region formActions
        private void NewDataBaseButtonClick(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = dbName;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                dbName = saveFileDialog1.FileName;
            }
            if (sql.OpenDB(dbName))
            {
                sql.CreateDB();
                toolStripStatusLabel.Text = "Leere Datenbank erstellt.";
            }
        }

        private void OpenButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "tsm.db3";
            if  (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                dbName = openFileDialog1.FileName;
            }
            if (sql.OpenDB(dbName))
            {
                toolStripStatusLabel.Text = "Datenbank geöffnet.";
                FillDistanzMatrix();
            }
        }

        private void ViewPointsButtonClick(object sender, EventArgs e)
        {
            dataGridView1.DataSource = sql.GetData("SELECT * FROM Stationen");
        }

        private void ViewDistancesButtonClick(object sender, EventArgs e)
        {
           dataGridView1.DataSource = sql.GetData("SELECT * FROM Distanzen");
        }

        private void AddPointButtonClick(object sender, EventArgs e)
        {
            string query;
            if (textBoxX.Text.Length > 0 && textBoxX.Text.Length > 0 && textBoxStationsName.Text.Length > 0)
            {
                query = "INSERT INTO Stationen (nr, X, Y, Typ) VALUES (null, '" + textBoxX.Text + "', '" + textBoxY.Text + "', '" + textBoxStationsName.Text + "');";
                sql.Query(query);
                dataGridView1.DataSource = sql.GetData("SELECT * FROM Stationen");
            }
            else
            {
                MessageBox.Show("Die Felder Nord, Ost und Name müssen gefüllt sein. Die Spielstationen müssen mit 'Station' anfangen gefolgt von einer Zahl.", "Hinweis", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void CalcDistancesButtonClick(object sender, EventArgs e)
        {
            /// Sicherheitsabfrage:
            dataGridView1.DataSource = sql.GetData("SELECT * FROM Distanzen");
            if (dataGridView1.Rows.Count > 1)
            {
                DialogResult dialogResult = MessageBox.Show("Es sind bereits Distanzen berechnet worden. Wollen Sie die Berechnung neu starten?", "Sicherheitsabfrage", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.No)
                    return;
            }

            /// erstmal alle Einträge löschen
            string query = "DELETE FROM Distanzen;";
            sql.Query(query);
            /// Abfrage der Punkte
            dataGridView1.DataSource = sql.GetData("SELECT * FROM Stationen");
            int countTemp = 0;
            double gesPunkte = (dataGridView1.Rows.Count - 2) * (dataGridView1.Rows.Count - 1) / 2.0;
            /// Äußere Schleife:
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                /// Innere Schleife:
                for (int j = i + 1; j < dataGridView1.Rows.Count - 1; j++)
                {
                    string distanzWanderweg = GetDistanz(i, j, true);
                    string distanzLuftlinie = GetDistanz(i, j, false);
                    query = "INSERT INTO Distanzen (Von, Nach, Distanz, Luftlinie) VALUES('" + i + "', '" + j + "', '" + distanzWanderweg + "', '" + distanzLuftlinie + "');";
                    sql.Query(query);
                    countTemp += 1;
                    toolStripStatusLabel.Text = "Verarbeite Distanz " + countTemp + "/" + gesPunkte + " von " + dataGridView1.Rows[i].Cells[3].Value + " nach " + dataGridView1.Rows[j].Cells[3].Value + " beträgt " + distanzWanderweg + "m. ";
                    toolStripProgressBar.Value = (int) (countTemp * 100.0 / gesPunkte);
                    statusStrip1.Update();
                    // MessageBox.Show("Distanz von " + dataGridView1.Rows[i].Cells[3].Value + " nach " + dataGridView1.Rows[j].Cells[3].Value + " beträgt " + dist + "m.");
                }
            }
            MessageBox.Show(((dataGridView1.Rows.Count - 2)*(dataGridView1.Rows.Count - 1)/2.0).ToString() + " Distanzen berechnet und in Datenbank geschrieben!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            FillDistanzMatrix();
        }

        /// Eine Option: rechnet nur "unsere Route" aus:
        private void CalcRouteButtonClick(object sender, EventArgs e)
        {
            // MessageBox.Show("Hier sollte Code stehen, der die Distanz (aus der DB) von 0 nach 1, 1 nach 2, usw. bis Station 8 und von dort wieder zum Zeltplatz berechnet, bzw. ausgiebt.");
            string queryString;
            double unsereDistanz = 0;
            double singleDistanz = 0;
            int letzteStation = 14;

            MessageBox.Show("Berechne Wegstrecke vom Zeltplatz über Station/Punkt: " + letzteStation + " und zurück.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            lCity = new List<City>(letzteStation);
            // Hinweis: das koennte auch über TourLenght berechnet werden!
            for (int i = 0; i < letzteStation; ++i)
            {
                queryString = "SELECT Distanz FROM Distanzen WHERE (Von = " + i + ") AND (Nach = " + (i + 1) + ");";
                dataGridView1.DataSource = sql.GetData(queryString);
                if (dataGridView1.DataSource == null)
                {
                    MessageBox.Show("Datenbank noch nicht geöffnet.", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (double.TryParse(dataGridView1.Rows[0].Cells[0].Value.ToString(), out singleDistanz))
                    unsereDistanz += singleDistanz;
                lCity.Add(new City(0.0, 0.0, i));
            }
            queryString = "SELECT Distanz FROM Distanzen WHERE (Von = " + 0 + ") AND (Nach = " + (letzteStation) + ");";
            dataGridView1.DataSource = sql.GetData(queryString);
            if (double.TryParse(dataGridView1.Rows[0].Cells[0].Value.ToString(), out singleDistanz))
                unsereDistanz += singleDistanz;
            ShowRoute(lCity, unsereDistanz / 1000.0);
            //MessageBox.Show("Gelaufene Wegstrecke: " + (unsereDistanz / 1000.0) + " km");
        }

        private void NavigateButtonClick(object sender, EventArgs e)
        {
            myBrowser.Navigate(textBoxUrl.Text);
            // myBrowser.Navigate("http://acid3.acidtests.org/");
        }


        /// Jetzt: TSM von https://code.msdn.microsoft.com/Traveling-Salesperson-79c7f2ba
        private void TSMallPointsButtonClick(object sender, EventArgs e)
        {
            int n = InitCityList(0);
            Tsm_sa_sdls(n);
        }

        private void TSMstationenButtonClick(object sender, EventArgs e)
        {
            int n = InitCityList(1);
            Tsm_sa_sdls(n);
        }

        private void TSMnextPointsButtonClick(object sender, EventArgs e)
        {
            int n = InitCityList(2);
            Tsm_sa_sdls(n);
        }

        /// Nur um auf Nummer sicher zu gehen ;)
        private void Fenster_FormClosing(Object sender, FormClosingEventArgs e)
        {
            sql.CloseDB();
            // MessageBox.Show("Datenbank gespeichert.");
        }

        #endregion



        #region eigeneKleineFunktionen

        private string GetDistanz(int i, int j, bool wanderWeg)
        {
            /// WARNING: das hier wird angenommen: dataGridView1.DataSource = sql.GetData("SELECT * FROM Stationen");
            double lat1, lon1, lat2, lon2;
            double.TryParse(dataGridView1.Rows[i].Cells[1].Value.ToString(), out lat1);
            double.TryParse(dataGridView1.Rows[i].Cells[2].Value.ToString(), out lon1);
            double.TryParse(dataGridView1.Rows[j].Cells[1].Value.ToString(), out lat2);
            double.TryParse(dataGridView1.Rows[j].Cells[2].Value.ToString(), out lon2);
            string posA = Regex.Replace(lon1.ToString(), ",", ".") + "," + Regex.Replace(lat1.ToString(), ",", ".");
            string posB = Regex.Replace(lon2.ToString(), ",", ".") + "," + Regex.Replace(lat2.ToString(), ",", ".");
            if (wanderWeg)
                return GetDirectionsWebClient(posA, posB);
            // distanceInKm ist etwas besser als einfach Wurzel aus dx^2 + dy^2
            return (DistanceInKmBetweenEarthCoordinates(lat1, lon1, lat2, lon2) * 1000.0).ToString();
        }

        string GetDirectionsWebClient(string positionA, string positionB)
        {
            using (var webClient = new System.Net.WebClient())
            {
                System.Threading.Thread.Sleep(1500); // Der freie Plan erlaubt 2.000 Punkte am Tag mit 40 pro Minute. D.h. es ergeben sich bei 26 Punkten 351 Distanzen knapp 9 Minuten, bei 30 Punkten bereits 465 Distanzen und damit fast 12 Minuten.
                //var baseAddress = new Uri("https://api.openrouteservice.org/directions?api_key=5b3ce3597851110001cf62487ce060c4f04c4868883f2aed4315ab67&coordinates=" + positionA + "|" + positionB + "&profile=foot-hiking");
                var baseAddress = new Uri("https://api.openrouteservice.org/v2/directions/foot-hiking?api_key=5b3ce3597851110001cf62487ce060c4f04c4868883f2aed4315ab67&start=" + positionA + "&end=" + positionB);
                webClient.BaseAddress = baseAddress.ToString();
                string webAnswer = webClient.DownloadString(baseAddress);
                return Regex.Match(webAnswer, "\"distance\":\\d+(.\\d+){0,1}").Value.Split(':')[1];
            }
        }

        /// Minimale MatheFibel:
        private double DegreesToRadians(double degrees) {
            return degrees* Math.PI / 180;
        }

        /// https://stackoverflow.com/questions/365826/calculate-distance-between-2-gps-coordinates
        private double DistanceInKmBetweenEarthCoordinates(double lat1, double lon1, double lat2, double lon2)
        {
            double earthRadiusKm = 6371;

            double dLat = DegreesToRadians(lat2 - lat1);
            var dLon = DegreesToRadians(lon2 - lon1);

            lat1 = DegreesToRadians(lat1);
            lat2 = DegreesToRadians(lat2);

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            return earthRadiusKm * c;
        }
        #endregion

        #region webFunktionen
        private Uri GenerateRouteLink(List<City> staedte)
        {
            // string url = "https://maps.openrouteservice.org/directions?";
            string url = "https://classic-maps.openrouteservice.org/directions?";
            dataGridView1.DataSource = sql.GetData("SELECT * FROM Stationen");
            double lat1, lon1;
            int cnt = 0;
            string posStart = "";
            /// Hier wäre auch mal echt ein clean-up möglich, denn die Citys haben ihre koordinaten auch unter c.x bzw. c.y gespeichert!
            foreach (City c in staedte)
            {
                double.TryParse(dataGridView1.Rows[c.Id].Cells[1].Value.ToString(), out lat1);
                double.TryParse(dataGridView1.Rows[c.Id].Cells[2].Value.ToString(), out lon1);
                string posA = Regex.Replace(lat1.ToString(), ",", ".") + "," + Regex.Replace(lon1.ToString(), ",", ".");
                if (cnt == 0)
                {
                    url += "n1=" + Regex.Replace(lat1.ToString(), ",", ".") + "&n2=" + Regex.Replace(lon1.ToString(), ",", ".") + "&n3=12&a=";
                    posStart = posA;
                }
                url += posA + ",";
                cnt++;
            }
            // Weg zurück zum Zeltplatz anhängen:
            url += posStart;
            // url += "&b=2&c=0&g1=-1&g2=0&k1=en-US&k2=km";   // alte Version
            url += "&b=2&c=0&k1=en-US&k2=km";
            return new Uri(url);
        }
        #endregion

        #region TSMCode
        // fügt alle Städte Distanzen der Matrix distTable hinzu
        private void FillDistanzMatrix()
        {
            dataGridView1.DataSource = sql.GetData("SELECT * FROM Stationen");
            int N = dataGridView1.Rows.Count - 1;

            City.InitDistTable(N);       // formeerly distTable = new double[N, N];
            double distanz = 0.0;
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    if (i == j)
                    {
                        City.distTable[i, j] = 0.0;
                        continue;
                    }
                    string queryString = "";       /// TODO der hier muss noch angepasst werden!
                    if (i < j)
                        queryString = "SELECT Distanz FROM Distanzen WHERE (Von = " + i + ") AND (Nach = " + j + ");";
                    else
                        queryString = "SELECT Distanz FROM Distanzen WHERE (Von = " + j + ") AND (Nach = " + i + ");";
                    dataGridView1.DataSource = sql.GetData(queryString);
                    // man könnte auch auf 0-Pointer von sql.GetData testen
                    try
                    {
                        double.TryParse(dataGridView1.Rows[0].Cells[0].Value.ToString(), out distanz);
                    }
                    catch (Exception)
                    {
                        distanz = 0.0;
                        MessageBox.Show("Zugriff auf Distanzmatrix hat nicht funktioniert.\n\nBitte berechnen Sie die Distanzen vollständig vor dem Ausführen von TSM-Berechnungen.", "Hinweis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                        // data = null;
                    }
                    City.distTable[i, j] = distanz;
                }
            }
        }

        private void AddCity(int i, ref List<int> nrListe, ref List<City> ortListe)
        {
            double x = 0.0;
            double y = 0.0;
            // int nr = 0;
            // int.TryParse(dataGridView1.Rows[i].Cells[0].Value.ToString(), out nr);
            // nrListe.Add(nr);
            double.TryParse(dataGridView1.Rows[i].Cells[1].Value.ToString(), out x);
            double.TryParse(dataGridView1.Rows[i].Cells[2].Value.ToString(), out y);
            ortListe.Add(new City(x, y, i));
        }

        private int InitCityList(int selectionType)
        {
            toolStripStatusLabel.Text = "Füge die Städte aus der Datenbank der Liste hinzu.";
            statusStrip1.Update();

            string queryString = "SELECT * FROM Stationen";
            dataGridView1.DataSource = sql.GetData(queryString);
            /// Alle Listen/Abfrage - Ergebnisse sollten so sein, dass man einfach darüber iterieren kann.
            int alle = dataGridView1.Rows.Count - 1;
            List<int> cityNrs = new List<int>();
            lCity = new List<City>();

            if (selectionType == 2)         ///< es sollen die "n" dem Startpunkt nächstgelgenen Stationen verwendet werden
            {
                dataGridView1.DataSource = sql.GetData("Select Distanz, Nach From Distanzen Where Von = '0' Order By 1;");
                int anzahlGewaehlterStationen = 0;
                int.TryParse(stationenAnzahlTextBox.Text.ToString(), out anzahlGewaehlterStationen);
                int nr = 0;
                for (int i = 0; i < anzahlGewaehlterStationen; i++)
                {
                    int.TryParse(dataGridView1.Rows[i].Cells[1].Value.ToString(), out nr);
                    cityNrs.Add(nr);
                }
                dataGridView1.DataSource = sql.GetData(queryString);
            }

            for (int i = 0; i < alle; i++)
            {
                switch (selectionType)
                {
                    case 1:
                        if (!dataGridView1.Rows[i].Cells[3].Value.ToString().Contains("StationT"))
                            AddCity(i, ref cityNrs, ref lCity);
                        break;
                    case 2:
                        if (cityNrs.Contains(i) || (i == 0))
                            AddCity(i, ref cityNrs, ref lCity);
                        break;
                    default:
                        AddCity(i, ref cityNrs, ref lCity);
                        break;
                }
            }

            return lCity.Count;
        }

        private void ShowRoute(List<City> Orte, double Streckenlaenge)
        {
            string queryString;
            string reinfolge = "";
            string routenLink = "";
            foreach (City c in Orte)
            {
                queryString = "SELECT Typ FROM Stationen WHERE Nr = " + (c.Id + 1) + ";";
                dataGridView1.DataSource = sql.GetData(queryString);
                reinfolge += dataGridView1.Rows[0].Cells[0].Value + " -> ";
            }
            queryString = "SELECT Typ FROM Stationen WHERE Nr = " + (1) + ";";
            dataGridView1.DataSource = sql.GetData(queryString);
            reinfolge += dataGridView1.Rows[0].Cells[0].Value;

            routenLink = GenerateRouteLink(Orte).ToString();
            // Das Anzeigen der Webseite will noch nicht so richtig.
            // TODO FIXME
            // webBrowser1.ScriptErrorsSuppressed = true;;          // wirklich viele Fehler...
            // webBrowser1.Navigate(new Uri(routenLink));              // der IE (der hier als Basis verwendet wird, packt die Seite nicht)
            myBrowser.Navigate(routenLink);
            textBoxUrl.Text = routenLink;
            try
            {
                Clipboard.SetData(DataFormats.Text, (Object) routenLink);
            } catch
            {
                MessageBox.Show("Fehler beim Kopieren in die Zwischenablage?!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            MessageBox.Show("Es sind: " + Orte.Count() + " Stationen, mit einer Länge von " + Streckenlaenge + "m über die folgenden Stationen:\n" + reinfolge, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            // MessageBox.Show(generateRouteLink(S));
        }

        private void Tsm_sa_sdls(int N)
        {
            // Stadt-Liste initialisieren geschieht in initVarsAndCitys
            int restarts = 10;
            toolStripStatusLabel.Text = N + " Stationen verarbeitet. Starte Berechnung mit " + restarts + " Durchgängen.";
            statusStrip1.Update();
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            // constants and init values:
            int count1, count2, count3, count4;
            double fS = 0, bestfS = double.MaxValue;
            City[] best, result;
            List<City> S = null;

            for (int i = 0; i < restarts; i++)
            {
                S = new List<City>(lCity);
                algos.SASDLS(N, 10000, 5000, 10, S.ToArray(), out result, out fS, out count1, out count2, out count3, out count4);
                S = new List<City>(result);

                if (fS < bestfS)
                {
                    bestfS = fS;
                    best = S.ToArray();
                }

                TimeSpan ts = sw.Elapsed;
                string elapsedtime = "";
                elapsedtime += ts.Hours.ToString("D2") + ":";
                elapsedtime += ts.Minutes.ToString("D2") + ":";
                elapsedtime += ts.Seconds.ToString("D2") + ".";
                elapsedtime += ts.Milliseconds.ToString("D3");

                toolStripStatusLabel.Text = "Durchgang " + (i+1) + " von " + restarts + " in " + elapsedtime + " berechnet. Akt. Strecke: " + (bestfS/1000.0) + "km";
                toolStripProgressBar.Value = (int) ((i + 1.0) * 100.0 / restarts);
                statusStrip1.Update();
            }

            // Ergebnis Darstellen:
            ShowRoute(S, bestfS);
        }



        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
          //  myBrowser.UpdateUrl(textBoxUrl);
        }
    }

    /* Hier ist mal kurz ein DB-Test:
string sql.Query = "INSERT INTO Artikel (id, name, url, parsed) VALUES (null, 'Toller Name', '" + textBoxURL.Text + "', 0);";
myDB.sql.Query(sql.Query);
sql.Query = "SELECT * FROM Artikel;";
dataGridView1.DataSource = myDB.sql.GetDataTable(sql.Query);         // aka wissen.getArtikelDatabase(myDB, sql.Query);
 */

}
